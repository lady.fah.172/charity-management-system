<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class FakeEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:fake';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete fake emails ';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        User::whereNull('name')->delete();
    }
}
