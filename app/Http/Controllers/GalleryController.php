<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\Like;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use function PHPUnit\Framework\isNull;

class GalleryController extends Controller
{
    public function add_image(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => "validator error"
            ], 400);
        }
        if ($request->hasFile('image')) {
            $request->image = $this->saveimage($request->image, 'gallery');

        }
        $gallery = Gallery::query()->create(['name'=>$request->name,
            'image'=>$request->image]);
        return response()->json(['message' => 'image added successfully'], 200);
    }

    public function show()
    {
        $like=Gallery::query()->leftJoin('likes','galleries.id','=','likes.image_id')
            ->selectRaw('galleries.*,count(likes.image_id) as count')
            ->groupBy('galleries.id')->get()->all();
        return response()->json(['gallery' => $like], 200);}

    public function like(Gallery $gallery)
    {
        $likes = Like::query()->where('image_id', $gallery['id'])->get();
        foreach ($likes as $like) {
            if ($like->user_id == Auth::id()) {
                $like->delete();
                return response()->json([
                    'delete likes' => count($likes) - 1
                ]);
            }

        }
        $like = new like();
        $like->image_id = $gallery['id'];
        $like->user_id = Auth::id();
        $like->save();
//        $gallery->update(['likes' => count($likes) + 1]);
        return response()->json(['likes' => count($likes) + 1]);
    }

    public function buy(Gallery $gallery)
    {
        if ($gallery['user_id']==null) {
            $gallery['user_id'] = Auth::id();
            $gallery->save();
            return response()->json(['message' => 'done']);
        }
        return response()->json(
            [
                'message' => 'sold'
            ]
        );
    }

}

