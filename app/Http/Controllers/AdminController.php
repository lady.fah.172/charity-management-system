<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    public function register(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'validator error'],400);
        }
        $input['password'] = bcrypt($input['password']);
        $user = Admin::create($input);
        $token = $user->createToken('ABC')->plainTextToken;

        return response()->json([
            'token' => $token,
            'message' => 'Admin sign up Successfully!'],200);
    }
    //__________________________________________________________________________________________________________________
 //login  in  user controller .
    public function logout()
    {
        Auth::guard('admin')->logout();
        return response()->json(['message' => "you have logout successfully"],200);}
}
