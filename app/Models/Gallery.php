<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;
    protected $table = "galleries";
    protected $fillable = ['user_id','name','image'];
    public $timestamps = false;


    //one to one:
    public function user()
    {
        return $this->belongsTo(User::class, 'image_id');
    }
    //one to many:
    public function like()
    {
        return $this->hasMany(Like::class);
    }

}
